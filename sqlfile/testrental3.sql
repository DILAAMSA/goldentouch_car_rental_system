-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3304
-- Generation Time: Jul 29, 2021 at 09:16 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testrental3`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `updationDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `UserName`, `Password`, `updationDate`) VALUES
(1, 'admin', '5c428d8875d2948607f3e3fe134d71b4', '2021-06-18 12:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `tblbooking`
--

CREATE TABLE `tblbooking` (
  `id` int(11) NOT NULL,
  `userEmail` varchar(100) DEFAULT NULL,
  `VehicleId` int(11) DEFAULT NULL,
  `FromDate` varchar(20) DEFAULT NULL,
  `ToDate` varchar(20) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `PostingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbooking`
--

INSERT INTO `tblbooking` (`id`, `userEmail`, `VehicleId`, `FromDate`, `ToDate`, `message`, `Status`, `PostingDate`) VALUES
(4, 'aienaqeer@gmail.com', 2, '21/11/2021', '21/11/2021', 'emergency', 0, '2021-06-25 13:32:51'),
(5, 'miemy.syamimi@gmail.com', 6, '24/07/2021', '26/07/2021', 'To visit the Kuala Lumpur city in two days and one night', 1, '2021-07-24 04:32:32'),
(6, 'Michael@gmail.com', 12, '24/11/2021', '25/11/2021', 'Notice me if the car is not available', 1, '2021-07-24 07:41:19'),
(7, 'aienaqeer@gmail.com', 11, '21/11/2021', '23/11/2021', 'Urgent trip ', 1, '2021-07-24 07:46:24'),
(8, 'athirahteddy67@gmail.com', 11, '12/10/2021', '16/10/2021', 'hope okay ', 0, '2021-07-24 09:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `tblbrands`
--

CREATE TABLE `tblbrands` (
  `id` int(11) NOT NULL,
  `BrandName` varchar(120) NOT NULL,
  `CreationDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbrands`
--

INSERT INTO `tblbrands` (`id`, `BrandName`, `CreationDate`, `UpdationDate`) VALUES
(8, 'PERODUA', '2021-07-01 01:32:38', NULL),
(9, 'HONDA', '2021-07-01 01:32:49', NULL),
(10, 'TOYOTA', '2021-07-01 01:33:45', NULL),
(11, 'PROTON', '2021-07-01 01:33:59', NULL),
(12, 'BMW', '2021-07-07 06:49:11', '2021-07-07 06:49:11');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontactusinfo`
--

CREATE TABLE `tblcontactusinfo` (
  `id` int(11) NOT NULL,
  `Address` tinytext,
  `EmailId` varchar(255) DEFAULT NULL,
  `ContactNo` char(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcontactusinfo`
--

INSERT INTO `tblcontactusinfo` (`id`, `Address`, `EmailId`, `ContactNo`) VALUES
(1, '3rd Floor, Block B, Lot 13A,\r\nJalan 219, Seksyen 51A,\r\n46100 Petaling Jaya,\r\nSelangor, Malaysia.																								', 'GoldenTouchCarSystem@gmail.com', '+6032345675');

-- --------------------------------------------------------

--
-- Table structure for table `tblcontactusquery`
--

CREATE TABLE `tblcontactusquery` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `EmailId` varchar(120) DEFAULT NULL,
  `ContactNumber` char(11) DEFAULT NULL,
  `Message` longtext,
  `PostingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcontactusquery`
--

INSERT INTO `tblcontactusquery` (`id`, `name`, `EmailId`, `ContactNumber`, `Message`, `PostingDate`, `status`) VALUES
(1, 'Harry Den', 'webhostingamigo@gmail.com', '2147483647', 'How to book the car', '2021-07-07 07:28:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblpages`
--

CREATE TABLE `tblpages` (
  `id` int(11) NOT NULL,
  `PageName` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  `detail` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpages`
--

INSERT INTO `tblpages` (`id`, `PageName`, `type`, `detail`) VALUES
(1, 'Terms and Conditions', 'terms', '																																																																																																																								<h5 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(62, 63, 58); text-align: justify;\"><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: medium; font-family: georgia;\">The use of this website is subject to the following terms of use:</span></h5><p align=\"justify\"></p><ol><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">The content of the pages of this website is for your general information and use only. It is subject to change without notice. You are advised to visit our website from time to time to ensure that you are happy with any changes.</span></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude any liability for any such inaccuracies or errors to the fullest extent permitted by law.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">All tradenames and trademarks reproduced in this website, which are not the property of, or licensed to us, are acknowledged on the website.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">Unauthorized use of this website may give to a claim for damages and/or a criminal offence.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">From time to time, this website may also include links to other websites including the GoldenTouch group of companies. These links are provided for your convenience to provide further information. Other than the GoldenTouch group of companies’ websites, we do not endorse other third parties’ websites. We have no responsibility for the content and/or security of the linked third parties’ website(s).</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\">You may not create a link to this website from another website or document without GoldenTouch Car Rentals’ prior written consent.</span><br></li><li style=\"text-align: justify;\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">Your use of this website and any dispute arising out of such use of the website is subject to the laws of Malaysia.</span></li></ol><p></p>\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										\r\n										'),
(2, 'Privacy Policy', 'privacy', '																				<p style=\"text-align: center; line-height: 1.6; color: rgb(0, 0, 0);\"><span style=\"font-size: medium; font-family: georgia;\">This privacy policy; in accordance with the Malaysian Personal Data Protection Act 2010; sets out how GoldenTouch Car Rental Portal uses and protects any information that you give Sime Darby Motors when you use this website.&nbsp;</span><span style=\"font-family: georgia; font-size: medium;\">GoldenTouch is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.&nbsp;</span><span style=\"font-family: georgia; font-size: medium;\">GoldenTouch may change this policy from time to time by updating this page. You should periodically visit this page from time to time to ensure that you are happy with any changes. This policy is effective from 1 January 2013.&nbsp;</span><span style=\"font-family: georgia; font-size: medium;\">This privacy policy applies to all information collected or submitted on this GoldenTouch Car Rental Portal.</span></p>\r\n										\r\n										'),
(3, 'About Us ', 'aboutus', '																																								<div style=\"text-align: center;\"><span style=\"color: rgb(0, 0, 0); font-family: georgia; font-size: medium;\"> Founded in 2021, GoldenTouch Malaysian\'s most distinguished rental car company. We provide customers with approximately more locations and vehicles throughout Malaysia. As we are not affiliated with any specific automaker, we are able to provide a variety of vehicle makes and models for customers to rent. Rather than let vehicles age, we also replace our most popular passenger vehicles every few years. Replacing used vehicles eliminates uncomfortable wear and tear, and reduces the risk of breakdown and other inconveniences to our customers. The most trusted name in vehicle rentals.</span></div>\r\n										\r\n										\r\n										\r\n										'),
(11, 'FAQs', 'faqs', '																														<h5 style=\"text-align: justify;\"><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: medium; font-family: georgia;\">Q1: Why should i rent a car in Goldentouch?</span></h5><p align=\"justify\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">Ans: Renting a car ahead via Goldentouch will save you some hassle, time and money. You can choose from our selection of cars from our trusted providers to go around the city. Book a car that suits your needs in Traveloka and you will enjoy instant confirmation at the best price.<h5 style=\"text-align: justify;\"><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: medium; font-family: georgia;\">Q2: How old do i have to be to rent a car?</span></h5><p align=\"justify\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">Ans: For most car hire companies, the age requirement is between 20 and 70 years old. If you\'re under 20 or over 70, you might have to pay an additional fee.</span></h5><p align=\"justify\"><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: medium; font-family: georgia;\">Q3: Can i book for car for someone else?</span></h5><p align=\"justify\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">Ans: Yes, as long as they meet these requirements. Just fill in their details while you\'re making the reservation.</span></h5><p align=\"justify\"><span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: medium; font-family: georgia;\">Q4: Can i book and get picked up on the same day?</span></h5><p align=\"justify\"><span style=\"color: rgb(0, 0, 0); font-size: medium; font-family: georgia;\">Ans: Yes you can! But you have to book the car at least 12 hours before your pick-up time.');

-- --------------------------------------------------------

--
-- Table structure for table `tblsubscribers`
--

CREATE TABLE `tblsubscribers` (
  `id` int(11) NOT NULL,
  `SubscriberEmail` varchar(120) DEFAULT NULL,
  `PostingDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubscribers`
--

INSERT INTO `tblsubscribers` (`id`, `SubscriberEmail`, `PostingDate`) VALUES
(1, 'anuj.lpu1@gmail.com', '2021-07-07 12:31:34'),
(2, 'aienaqeer@gmail.com', '2021-07-29 09:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbltestimonial`
--

CREATE TABLE `tbltestimonial` (
  `id` int(11) NOT NULL,
  `UserEmail` varchar(100) NOT NULL,
  `Testimonial` mediumtext NOT NULL,
  `PostingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltestimonial`
--

INSERT INTO `tbltestimonial` (`id`, `UserEmail`, `Testimonial`, `PostingDate`, `status`) VALUES
(1, 'test@gmail.com', 'The service is so awesome.', '2021-07-24 04:10:38', 1),
(2, 'miemy.syamimi@gmail.com', 'Wow so comfortable and car very clean. I\'m so satisfied with the service.', '2021-07-24 04:36:18', 1),
(3, 'aienaqeer@gmail.com', 'Advertisement was clear and precise, cars all look very strong and in good condition. Prices are reasonable and it was very easy to place order. I will surely recommend this car rental to my family and friends in the future. ', '2021-07-24 05:47:39', 1),
(4, 'Michael@gmail.com', 'Everything was good with my trip, I am very glad that i booked car from GoldenTouch for my first time and sure wont be the last cause is very easy to take and return the car.', '2021-07-24 07:43:03', 1),
(5, 'athirahteddy67@gmail.com', 'The car is in good condition as well as very clean and comfortable to drive. I love it so much .', '2021-07-24 09:21:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `id` int(11) NOT NULL,
  `FullName` varchar(120) DEFAULT NULL,
  `EmailId` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `ContactNo` char(11) DEFAULT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `Country` varchar(100) DEFAULT NULL,
  `RegDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `FullName`, `EmailId`, `Password`, `ContactNo`, `dob`, `Address`, `City`, `Country`, `RegDate`, `UpdationDate`) VALUES
(4, 'Tom K', 'test@gmail.com', '5c428d8875d2948607f3e3fe134d71b4', '9999857868', '', 'PKL', 'XYZ', 'XYZ', '2021-07-07 12:30:28', '2021-07-07 12:30:28'),
(5, 'Aina', 'aienaqeer@gmail.com', '745c38cb7dc6dfae845efd52e2c2d005', '0196192051', '', '', '', '', '2021-07-06 12:26:43', '2021-07-24 07:30:44'),
(6, 'Syamimi', 'miemy.syamimi@gmail.com', '25d55ad283aa400af464c76d713c07ad', '+10191998', '', '', '', '', '2021-07-24 04:27:47', '2021-07-24 04:38:30'),
(7, 'Michael', 'Michael@gmail.com', 'd2472fc2fa64f9284b34237923d206d4', '0192628539', NULL, NULL, NULL, NULL, '2021-07-24 07:35:18', NULL),
(8, 'nurul athirah ', 'athirahteddy67@gmail.com', '085e0ea39b1b07b087f9c934cc66d168', '0162183052', NULL, NULL, NULL, NULL, '2021-07-24 09:12:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblvehicles`
--

CREATE TABLE `tblvehicles` (
  `id` int(11) NOT NULL,
  `VehiclesTitle` varchar(150) DEFAULT NULL,
  `VehiclesBrand` int(11) DEFAULT NULL,
  `VehiclesOverview` longtext,
  `PricePerDay` int(11) DEFAULT NULL,
  `FuelType` varchar(100) DEFAULT NULL,
  `ModelYear` int(6) DEFAULT NULL,
  `SeatingCapacity` int(11) DEFAULT NULL,
  `Vimage1` varchar(120) DEFAULT NULL,
  `Vimage2` varchar(120) DEFAULT NULL,
  `Vimage3` varchar(120) DEFAULT NULL,
  `Vimage4` varchar(120) DEFAULT NULL,
  `Vimage5` varchar(120) DEFAULT NULL,
  `AirConditioner` int(11) DEFAULT NULL,
  `PowerDoorLocks` int(11) DEFAULT NULL,
  `AntiLockBrakingSystem` int(11) DEFAULT NULL,
  `BrakeAssist` int(11) DEFAULT NULL,
  `PowerSteering` int(11) DEFAULT NULL,
  `DriverAirbag` int(11) DEFAULT NULL,
  `PassengerAirbag` int(11) DEFAULT NULL,
  `PowerWindows` int(11) DEFAULT NULL,
  `CDPlayer` int(11) DEFAULT NULL,
  `CentralLocking` int(11) DEFAULT NULL,
  `CrashSensor` int(11) DEFAULT NULL,
  `LeatherSeats` int(11) DEFAULT NULL,
  `RegDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblvehicles`
--

INSERT INTO `tblvehicles` (`id`, `VehiclesTitle`, `VehiclesBrand`, `VehiclesOverview`, `PricePerDay`, `FuelType`, `ModelYear`, `SeatingCapacity`, `Vimage1`, `Vimage2`, `Vimage3`, `Vimage4`, `Vimage5`, `AirConditioner`, `PowerDoorLocks`, `AntiLockBrakingSystem`, `BrakeAssist`, `PowerSteering`, `DriverAirbag`, `PassengerAirbag`, `PowerWindows`, `CDPlayer`, `CentralLocking`, `CrashSensor`, `LeatherSeats`, `RegDate`, `UpdationDate`) VALUES
(6, 'Perodua Myvi', 8, 'Bodytype Hatchback', 70, 'Petrol', 2021, 5, 'Perodua Myvi.png', 'perodua myvi back.jfif', 'perodua myvi interior.jpg', 'Perodua-Myvi-1.5-EXTREME-15-Interior.jpg', '', 1, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '2021-07-01 01:36:06', NULL),
(7, 'Poton Saga', 11, 'Body Type Sedan', 80, 'Petrol', 2021, 5, 'Proton saga.png', 'proton saga back.jpg', 'proton saha interior.jpg', 'proton saga seat.jpg', '', 1, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, '2021-07-01 01:44:46', NULL),
(8, 'Honda City RS', 9, 'Body Type Sedan ', 100, 'Petrol', 2020, 5, 'honda city.jpg', 'honda city back.jpg', 'honda city interior.jpg', 'Honda-City-2020-rear-seats.jpg', '', 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 01:51:52', NULL),
(9, 'VIOS', 10, 'Body Type Sedan', 100, 'Petrol', 2019, 5, 'Toyota Vios.jpg', 'toyota vios back.jpg', 'Toyota vios interior.jpg', 'toyota vios back seat.jpg', '', 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, '2021-07-06 14:28:00', '2021-07-06 15:09:41'),
(10, 'Corolla Cross', 10, 'BodyType SUV\r\nHorsepower 139PS', 150, 'Petrol', 2021, 5, 'honda corrola.jpg', 'honda corrola back.jpg', 'honda corrola interior.jpg', 'honda corrolaa back.jpeg', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-07-22 12:59:43', NULL),
(11, 'Bezza', 8, 'BodyType Sedan\r\nHorsepower 69 - 96PS', 90, 'Petrol', 2020, 5, 'proton bezza face.jpg', 'Proton bezza.jpg', 'perodua bezza interior.webp', 'perodua bezza back.jpg', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-07-22 13:03:35', NULL),
(12, 'x50', 11, 'BodyType SUV\r\nHorsepower 150 - 177PS', 100, 'Petrol', 2020, 5, 'proton x50.jpg', 'proton x50 back.png', 'x50 interior.webp', 'x50 seat.jpg', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-07-22 14:31:06', NULL),
(13, 'Yaris', 10, 'BodyType Hatchback\r\nHorsepower 107PS', 85, 'Petrol', 2020, 5, 'yaris.jpg', 'yaris back.jpg', 'yaris interior.jpg', 'yaris seat.jpg', '', 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, NULL, '2021-07-22 14:33:24', NULL),
(14, 'Civic', 9, 'BodyType Sedan\r\nHorsepower 141 - 173PS', 90, 'Petrol', 2020, 5, 'Honda civic.jpg', 'honda civic back.jpg', 'honda civic interior.jpg', 'honda civic seat.jpg', '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2021-07-22 14:35:37', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbooking`
--
ALTER TABLE `tblbooking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblbrands`
--
ALTER TABLE `tblbrands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcontactusinfo`
--
ALTER TABLE `tblcontactusinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcontactusquery`
--
ALTER TABLE `tblcontactusquery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpages`
--
ALTER TABLE `tblpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsubscribers`
--
ALTER TABLE `tblsubscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltestimonial`
--
ALTER TABLE `tbltestimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblvehicles`
--
ALTER TABLE `tblvehicles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblbooking`
--
ALTER TABLE `tblbooking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblbrands`
--
ALTER TABLE `tblbrands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tblcontactusinfo`
--
ALTER TABLE `tblcontactusinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcontactusquery`
--
ALTER TABLE `tblcontactusquery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblpages`
--
ALTER TABLE `tblpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tblsubscribers`
--
ALTER TABLE `tblsubscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbltestimonial`
--
ALTER TABLE `tbltestimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tblvehicles`
--
ALTER TABLE `tblvehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
