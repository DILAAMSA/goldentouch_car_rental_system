<?php

if(isset($_POST['emailsubscibe']))
{
$subscriberemail=$_POST['subscriberemail'];
$sql ="SELECT SubscriberEmail FROM tblsubscribers WHERE SubscriberEmail=:subscriberemail";
$query= $dbh -> prepare($sql);
$query-> bindParam(':subscriberemail', $subscriberemail, PDO::PARAM_STR);
$query-> execute();
$results = $query -> fetchAll(PDO::FETCH_OBJ);
$cnt=1;

if($query -> rowCount() > 0)
{
echo "<script>alert('Already Subscribed.');</script>";
}
else{
$sql="INSERT INTO  tblsubscribers(SubscriberEmail) VALUES(:subscriberemail)";
$query = $dbh->prepare($sql);
$query->bindParam(':subscriberemail',$subscriberemail,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
echo "<script>alert('Subscribed successfully.');</script>";
}
else
{
echo "<script>alert('Something went wrong. Please try again');</script>";
}
}
}

?>

<footer>
  <div class="footer-top">
    <div class="container">
      <div class="row">

        <div class="col-md-6">
          <h6>QUICK LINK</h6>
          <ul>


          <li><a href="page.php?type=aboutus">About Us</a></li>
            <li><a href="page.php?type=faqs">FAQs</a></li>
            <li><a href="page.php?type=privacy">Privacy</a></li>
          <li><a href="page.php?type=terms">Terms of use</a></li>
               <li><a href="admin/">Admin Login</a></li>
          </ul>
        </div>

        <div class="col-md-3 col-sm-6">
          <h6>Subscribe Newsletter</h6>
          <div class="newsletter-form">
            <form action="index.php" method="post">
              <?php
              $subscriberemail = "";
              if(isset($_POST['emailsubscibe'])){
                $subscriberemail = $_POST['subscriberemail'];
                if (filter_var($subscriberemail, FILTER_VALIDATE_EMAIL)){
                  
                  $subject = "Thanks for subscribing us-GOLDENTOUCH";
                  $message = "Thank for subscribing our website. You will always receive latest update from us and dont worry we wont share or sell your information to anyone";
                  $sender = "From: GoldenTouchCarSystem@gmail.com";
                  if(mail($subscriberemail,  $subject, $message, $sender)){
                    ?>
                    <div class = "alert success">Thanks for subscribing us. </div>
                    
                    <?php

                    $subscriberemail = "";

                  }else{
                    ?>
                    <div class = "alert error">Failed while sending your email. </div>
                    <?php
                    $subscriberemail = "";

                  }

                }else {
                  ?>
                  <div class="alert error">invalid email</div>
                  <?php
                  

                }
              }
              ?>
              <div class="form-group">
                <input type="email" name="subscriberemail" class="form-control newsletter-input" required placeholder="Enter Email Address" required value="<?php echo $subscriberemail ?>" />
              </div>
              <button type="submit" name="emailsubscibe" class="btn btn-block">Subscribe <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
            </form>
            <p class="subscribed-text">*We send great deals and latest auto news to our subscribed users very week.</p>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-6 text-right">
          <div class="footer_widget">
          </div>
        </div>
        <div class="col-md-6 col-md-pull-6">
          <p class="copy-right">Copyright &copy; 2021 Golden Touch Car Rental Portal.</p>
        </div>
      </div>
    </div>
  </div>
</footer>
