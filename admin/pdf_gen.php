<?Php
include('pdf/fpdf.php');
$dbh = new PDO('mysql:host=localhost:3304;dbname=testrental3', 'root','');

class myPDF extends FPDF{
    function header (){
        
        $this->SetFont ('Arial', 'B', 14);
        $this->Cell(276,5, 'GOLDENTOUCH CAR RENTAL SYSTEM',0,0,'C');
        $this->Ln();
        $this->setFont('Times','',12);
        $this->Cell(276,10, 'Customers Booking Report',0,0,'C');
        $this->Ln(20);
    }

    function footer(){
        $this->SetY(-15);
        $this->SetFont ('Arial','',8);
        $this->Cell(0,10,'Page '. $this->PageNo().'/{nb}',0,0,'C');
    }
    function headerTable(){
        $this->SetFont ('Times','B',11);
        $this->Cell(50,10,'NAME',1,0,'C');
        $this->Cell(40,10,'VEHICLE TITLE',1,0,'C');
        $this->Cell(30,10,'BRAND',1,0,'C');
        $this->Cell(30,10,'FROM DATE',1,0,'C');
        $this->Cell(30,10,'TO DATE',1,0,'C');
        $this->Cell(100,10,'MESSAGE',1,0,'C');
        $this->Ln();

    }
    function viewTable($dbh){
        $this->SetFont ('Times', '', 11);
        $stmt = $dbh ->query("SELECT tblusers.FullName,tblbrands.BrandName,tblvehicles.VehiclesTitle,tblbooking.FromDate,tblbooking.ToDate,tblbooking.message,tblbooking.VehicleId as vid,tblbooking.Status,tblbooking.PostingDate,tblbooking.id  from tblbooking join tblvehicles on tblvehicles.id=tblbooking.VehicleId join tblusers on tblusers.EmailId=tblbooking.userEmail join tblbrands on tblvehicles.VehiclesBrand=tblbrands.id  ");
        while($data = $stmt->fetch(PDO::FETCH_OBJ)){
            $this->Cell(50,10,$data->FullName,1,0,'L');
            $this->Cell(40,10,$data->VehiclesTitle,1,0,'L');
            $this->Cell(30,10,$data->BrandName,1,0,'L');
            $this->Cell(30,10,$data->FromDate,1,0,'L');
            $this->Cell(30,10,$data->ToDate,1,0,'L');
            $this->Cell(100,10,$data->message,1,0,'L');
            $this->Ln();

        }
       
            
        
           

        

    }

}

$pdf = new myPDF ();
$pdf -> AliasNbPages();
$pdf ->AddPage ('L', 'A4', 0);
$pdf-> headerTable();
$pdf ->viewTable($dbh);
$pdf-> Output();

?>